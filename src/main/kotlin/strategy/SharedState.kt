package strategy

import model.LootBox

val unitToWeapon: MutableMap<Int, LootBox> = HashMap()

val unitToEnemy: MutableMap<Int, model.Unit> = HashMap()