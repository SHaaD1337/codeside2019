package strategy

import debugPackage.drawDebug
import model.UnitAction
import strategy.behaviourTree.movementNodes.updateGraphScore
import strategy.behaviourTree.tree

class MyStrategy {

    fun getAction(unit: model.Unit): UnitAction {
        updateGraphScore(unit)
        drawDebug(unit)
        return tree.eval(unit)
    }

}
