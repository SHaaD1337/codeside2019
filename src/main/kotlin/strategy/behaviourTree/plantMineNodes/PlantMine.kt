package strategy.behaviourTree.plantMineNodes

import model.Unit
import model.UnitAction
import strategy.behaviourTree.BTreeNode
import strategy.enemyUnits
import strategy.extensions.distance
import strategy.math.noObstaclesToEnemy

object PlantMine : BTreeNode() {
    val isApplicable: (Unit) -> Boolean = { unit: Unit ->
        enemyUnits.any { unit.distance(it) < 45.0 } && enemyUnits.all {
            !noObstaclesToEnemy(unit.position, it.position)
        }
    }

    override fun eval(unit: Unit): UnitAction {
        return UnitAction(plantMine = true)
    }

}
