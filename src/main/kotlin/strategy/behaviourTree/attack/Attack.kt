package strategy.behaviourTree.attack

import model.Unit
import model.UnitAction
import model.minus
import strategy.ally
import strategy.math.hasObstaclesBetween
import strategy.math.intersectsLineRect
import strategy.math.intersectsPointTriangle
import strategy.math.rotatePoint

object Attack : AttackBase() {
    override fun eval(unit: Unit): UnitAction {
        val targetUnit = findTargetUnit(unit)
        val target = targetUnit.center

        val noAllyOnFire = ally(unit)?.let { ally ->
            !intersectsLineRect(unit.center, target, ally.bottomLeftPos, ally.size) &&
                    (unit.weapon?.lastAngle == null ||
                            !intersectsPointTriangle(
                                    ally.center,
                                    unit.center,
                                    rotatePoint(unit.center, unit.weapon!!.lastAngle!! + unit.weapon!!.spread / 2.0, 1000.0),
                                    rotatePoint(unit.center, unit.weapon!!.lastAngle!! - unit.weapon!!.spread / 2.0, 1000.0)
                            ))
        } ?: true

        val targetInsideAttackSector = if (unit.weapon?.lastAngle == null) false else {
            targetInsideSpreadSector(target, unit)
        }

        val shouldShoot = !hasObstaclesBetween(unit.center, target) &&
                noAllyOnFire && targetInsideAttackSector

        val aim = if (!targetInsideAttackSector) {
            target
        } else {
            rotatePoint(unit.center, unit.weapon?.lastAngle ?: 0.0, 5.0)
        }

        return UnitAction(
                aim = aim - unit.center,
                shoot = shouldShoot
        )
    }

}
