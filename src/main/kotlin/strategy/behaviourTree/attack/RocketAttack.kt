package strategy.behaviourTree.attack

import debugPackage.debug
import model.*
import model.Unit
import strategy.bullets.bulletCollidesWithTiles
import strategy.bullets.getBulletVelocity
import strategy.extensions.perTick
import strategy.math.hasObstaclesBetween
import strategy.math.intersects
import strategy.math.rotatePoint
import strategy.myPlayerId
import strategy.properties
import strategy.units

object RocketAttack : AttackBase() {
    val isApplicable = { unit: Unit -> unit.weapon?.type == WeaponType.ROCKET_LAUNCHER }

    override fun eval(unit: Unit): UnitAction {
        val target = findTargetUnit(unit).center

        val lastAngle = unit.weapon?.lastAngle ?: 0.0

        val shouldShoot = unit.weapon?.lastAngle != null &&
                intersectsWithSomethingInSpread(lastAngle, unit)
                && !hasObstaclesBetween(unit.center, target)

        val aim = if (!targetInsideSpreadSector(target, unit)) {
            target
        } else {
            rotatePoint(unit.center, lastAngle, 5.0)
        }

        return UnitAction(
                aim = aim - unit.center,
                shoot = shouldShoot
        )
    }

    private fun intersectsWithSomethingInSpread(lastAngle: Double, unit: Unit): Boolean {
        val spreadDiff = unit.weapon!!.spread / 5.0

        var currentAngle = lastAngle - unit.weapon!!.spread
        while (currentAngle <= lastAngle + unit.weapon!!.spread) {
            val ticksToConsider = 10
            val bulletParams = unit.weapon!!.params.bullet
            val velocity = getBulletVelocity(unit, currentAngle, unit.weapon!!.params.bullet.speed.perTick())
            val explosionRadius = properties.weaponParams[WeaponType.ROCKET_LAUNCHER]!!.explosion!!.radius

            for (i in 0..ticksToConsider) {
                val bullet = Bullet(position = unit.center + velocity * i * 2, velocity = velocity, size = bulletParams.size,
                        unitId = unit.id)

                debug.drawPoint(bullet.position)

                val hitUnits = if (bulletCollidesWithTiles(bullet) ||
                        units.filter { it.id != unit.id }
                                .any { intersects(it.bottomLeftPos, it.size, bullet.bottomLeft, bullet.vecSize) }) {
                    units.filter {
                        val pos = if (it.playerId == myPlayerId) it.bottomLeftPos - it.size else it.bottomLeftPos
                        val size = if (it.playerId == myPlayerId) it.size * 2 else it.size
                        intersects(pos, size, bullet.position - explosionRadius, Vec2Double(explosionRadius * 2))
                    }
                } else emptyList()

                if (hitUnits.any { myPlayerId == it.playerId }) {
                    return false
                }
            }
            currentAngle += spreadDiff
        }
        return true
    }
}
