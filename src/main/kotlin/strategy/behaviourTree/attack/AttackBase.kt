package strategy.behaviourTree.attack

import model.Unit
import model.Vec2Double
import model.mY
import model.pY
import strategy.behaviourTree.BTreeNode
import strategy.enemyUnits
import strategy.extensions.distance
import strategy.math.hasObstaclesBetween
import strategy.math.intersectsPointTriangle
import strategy.math.rotatePoint
import strategy.properties

abstract class AttackBase : BTreeNode() {
    protected fun targetInsideSpreadSector(target: Vec2Double, unit: Unit): Boolean = intersectsPointTriangle(
            target,
            unit.center,
            rotatePoint(unit.center, (unit.weapon!!.lastAngle ?: 0.0) + unit.weapon!!.spread, 1000.0),
            rotatePoint(unit.center, (unit.weapon!!.lastAngle ?: 0.0) - unit.weapon!!.spread, 1000.0)
    )

    protected fun findTargetUnit(unit: Unit): Unit {
        var units = enemyUnits.filter { !hasObstaclesBetween(unit.center, it.center) }
        if (units.isEmpty()) {
            units = enemyUnits
        }
        return units.minBy { unit.distance(it) }!!
    }

}