package strategy.behaviourTree.movementNodes

import model.Unit
import model.UnitAction
import strategy.behaviourTree.BTreeNode
import strategy.behaviourTree.evasion.MoveWithEvasion
import strategy.extensions.distance
import strategy.minePacks

object GoForMine : BTreeNode() {

    val isApplicable: (Unit) -> Boolean = { unit: Unit ->
        minePacks.isNotEmpty() &&
                minePacks.asSequence()
                        .map { it to unit.distance(it) }
                        .minBy { it.second }!!.second < 20.0
    }

    override fun eval(unit: Unit): UnitAction = MoveWithEvasion { unit ->
        minePacks.minBy { unit.distance(it) }!!.position
    }.eval(unit)
}
