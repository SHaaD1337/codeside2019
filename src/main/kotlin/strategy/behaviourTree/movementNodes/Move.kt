package strategy.behaviourTree.movementNodes

import model.Unit
import model.UnitAction
import model.Vec2Double
import strategy.behaviourTree.BTreeNode
import strategy.extensions.perTick
import strategy.properties


class Move(val destination: (Unit) -> Vec2Double) : BTreeNode() {
    override fun eval(unit: Unit): UnitAction {
        val dest = destination(unit)

        val route = findRoute(unit.position, dest, unit)

        if (route.isEmpty()) return UnitAction()

        val destVertexCoord = route[0].coord

        val jump = destVertexCoord.y > unit.position.y
        val jumpDown = (unit.onGround || unit.onLadder) && destVertexCoord.y - unit.position.y < -0.1

        val velocityMultiplier = if (destVertexCoord.x - unit.position.x > 0) 1.0 else -1.0

        return UnitAction(
                velocity = getVelocity(Math.abs(destVertexCoord.x - unit.position.x)) * velocityMultiplier,
                jump = jump,
                jumpDown = jumpDown
        )
    }

    private fun getVelocity(distance: Double): Double =
            if (distance > properties.unitMaxHorizontalSpeed.perTick()) properties.unitMaxHorizontalSpeed
            else distance * properties.ticksPerSecond
}
