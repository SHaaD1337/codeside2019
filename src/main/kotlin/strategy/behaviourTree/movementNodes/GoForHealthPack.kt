package strategy.behaviourTree.movementNodes

import model.Unit
import model.UnitAction
import strategy.behaviourTree.BTreeNode
import strategy.behaviourTree.evasion.MoveWithEvasion
import strategy.extensions.distance
import strategy.healthPacks
import strategy.math.distanceSqr
import strategy.properties

object GoForHealthPack : BTreeNode() {

    val isApplicable: (Unit) -> Boolean = { unit: Unit ->
        healthPacks.isNotEmpty() && unit.health * 1.0 / properties.unitMaxHealth < 0.95
    }

    override fun eval(unit: Unit): UnitAction = MoveWithEvasion { unit ->
        healthPacks.minBy { unit.distance(it) }!!.position
    }.eval(unit)
}
