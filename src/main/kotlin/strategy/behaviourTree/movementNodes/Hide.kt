package strategy.behaviourTree.movementNodes

import model.Unit
import model.UnitAction
import model.pY
import strategy.behaviourTree.BTreeNode
import strategy.behaviourTree.evasion.MoveWithEvasion
import strategy.enemyUnits
import strategy.extensions.distance
import strategy.graph
import strategy.math.hasObstaclesBetween

object Hide : BTreeNode() {
    val isApplicable = { unit: Unit -> unit.isReloading() }

    override fun eval(unit: Unit): UnitAction = MoveWithEvasion {
        graph.vertexes.filter { v ->
            enemyUnits.all {
                hasObstaclesBetween(v.coord, it.center)
            }
        }.minBy { unit.distance(it) }?.coord ?: it.position.pY(5.0)
    }.eval(unit)
}