package strategy.behaviourTree.movementNodes

import model.Unit
import model.UnitAction
import model.Vec2Double
import model.pY
import strategy.*
import strategy.behaviourTree.BTreeNode
import strategy.behaviourTree.evasion.MoveWithEvasion
import strategy.math.hasObstaclesBetween
import strategy.math.intersectsLineRect
import strategy.math.manhattanDistance
import java.lang.Double.min

object MoveTowardsFiringPosition : BTreeNode() {

    private val action = MoveWithEvasion { unit ->
        val distance = 10.0
        val maxDistance = 30.0
        graph.vertexes
                .asSequence()
                .filter {
                    enemyUnits.map { enemy -> manhattanDistance(enemy.position, it.coord) }
                            .min()!! in (distance..maxDistance)
                }
                .filter { v -> mines.all { manhattanDistance(v.coord, it.position) > properties.mineExplosionParams.radius * 4.0 } }
                .toList()

                // don't stay near ally
                .filter { v ->
                    val otherUnit = ally(unit) ?: return@filter true
                    manhattanDistance(otherUnit.center, v.coord) > 10.0
                }

                //should be able to shoot enemy && not hit ally
                .let { c ->
                    c.filter {
                        enemyUnits.any { enemy ->
                            val canShootEnemy = !hasObstaclesBetween(it.coord.pY(properties.unitSize.y / 2.0), enemy.center)
                            val noUnitsOnFireLine = ally(unit)?.let { ally ->
                                !intersectsLineRect(unit.center, enemy.center, ally.bottomLeftPos, ally.size)
                            } ?: true

                            canShootEnemy && noUnitsOnFireLine
                        }
                    }.let { if (it.isNotEmpty()) it else c }
                }
                .minBy {
                    manhattanDistance(it.coord, unit.position) + distanceToBorder(it.coord)
                }?.coord
                ?: graph.vertexes.filter {
                    enemyUnits.map { enemy -> manhattanDistance(enemy.position, it.coord) }
                            .min()!! > 15.0
                }.minBy {
                    manhattanDistance(it.coord, unit.position) + distanceToBorder(it.coord)
                }!!.coord
    }

    override fun eval(unit: Unit): UnitAction = action.eval(unit)

    private fun distanceToBorder(vec: Vec2Double): Double =
            level.tiles.size / 2.0 - min(level.tiles.size - vec.x, vec.x)
}

