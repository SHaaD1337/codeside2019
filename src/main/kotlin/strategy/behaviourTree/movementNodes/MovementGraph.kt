package strategy.behaviourTree.movementNodes

import model.*
import model.Unit
import strategy.*
import strategy.bullets.bulletsIntersectingWithUnitAtPosition
import strategy.bullets.explosionsIntersectingWithUnitAtPosition
import strategy.extensions.distance


class MovementGraph(level: Level) {
    val edges: MutableMap<Vertex, MutableMap<Vertex, Edge>> = mutableMapOf()
    val vertexes: List<Vertex>

    init {
        vertexes = level.tiles.mapIndexed { x, tiles ->
            val minY = tiles.mapIndexed { y, t -> y to t }
                    .filter { it.second == Tile.WALL }
                    .minBy { it.first }
                    ?.first ?: return@mapIndexed listOf<Vertex>()
            (minY + 1 until tiles.size)
                    .filter { tiles[it] != Tile.WALL && it < tiles.size - 1 && tiles[it + 1] != Tile.WALL }
                    .map { y -> Vertex(Vec2Double(x + 0.5, y), tiles[y]) }
        }.flatten()

        val coord2Vertex = vertexes.associateBy { it.coord }

        fun addEdge(vertex: Vertex, v2: Vertex) =
                edges.computeIfAbsent(vertex) { mutableMapOf() }
                        .put(v2, Edge())

        vertexes.forEach { v ->
            val (x, y) = v.tileCoordinates

            fun handleBottom() {
                val bottomVertex = coord2Vertex[Vec2Double(x + 0.5, y - 1)]
                if (bottomVertex != null && level.tiles[x][y - 1] != Tile.WALL)
                    addEdge(v, bottomVertex)
            }

            fun handleBottomShift(xShift: Int) {
                val bottomShift = coord2Vertex[Vec2Double(x + 0.5 + xShift, y - 1)]
                val shift = coord2Vertex[Vec2Double(x + 0.5 + xShift, y)]

                val empty2Ladder = v.tile == Tile.EMPTY && v.tileUnder == Tile.EMPTY &&
                        (level.tiles[x + xShift][y] == Tile.LADDER || level.tiles[x + xShift][y] == Tile.PLATFORM)

                if (shift != null && level.tiles[x + xShift][y - 1] != Tile.EMPTY && !empty2Ladder) {
                    addEdge(v, shift)
                }
                if (bottomShift != null) {
                    addEdge(v, bottomShift)
                }
            }

            fun handleTop() {
                val topVertex = coord2Vertex[Vec2Double(x + 0.5, y + 1)]
                if (topVertex != null)
                    addEdge(v, topVertex)
            }

            fun handleTopShift(xShift: Int) {
                val topShift = coord2Vertex[Vec2Double(x + 0.5 + xShift, y + 1)]

                if (level.tiles[x].size > y + 2) {
                    val tile = level.tiles[x][y + 2]
                    if (tile == Tile.WALL) return
                }

                if (topShift != null) addEdge(v, topShift)
            }

            handleTop()
            handleTopShift(1)
            handleTopShift(-1)
            handleBottom()
            handleBottomShift(-1)
            handleBottomShift(1)
        }

    }
}

data class Vertex(val coord: Vec2Double, val tile: Tile,
                  val tileUnder: Tile = level.tiles[coord.x.toInt()][coord.y.toInt() - 1]) {

    var damageImpactScore = 0.0
    var otherUnitsCollisionScore = 0.0

    val tileCoordinates = coord.x.toInt() to coord.y.toInt()

    fun tileLeft() = level.tiles[coord.x.toInt() - 1][coord.y.toInt()]
    fun tileRight() = level.tiles[coord.x.toInt() + 1][coord.y.toInt()]
}

class Edge

fun updateGraphScore(unit: Unit) {
    graph.vertexes
            .onEach { it.damageImpactScore = 0.0 }
            .onEach { it.otherUnitsCollisionScore = intersectionWithUnitsScore(unit, it.coord) }
            .filter { unit.distance(it) <= 20.0 }
            .forEach { v ->
                v.damageImpactScore = damageScoreForUnit(v, unit)
            }
}

private fun damageScoreForUnit(v: Vertex, unit: Unit): Double {
    val unitPosition = v.coord.copy(x = v.coord.x - unit.size.x / 2.0)
    var score =
            bulletsIntersectingWithUnitAtPosition(unit, v.coord)
                    .sumByDouble { it.damage.toDouble() * 2 }

    score +=
            bulletsIntersectingWithUnitAtPosition(unit, unitPosition - unit.size, unit.size * 2.0)
                    .sumByDouble { it.damage * 1.5 }

    score +=
            bulletsIntersectingWithUnitAtPosition(unit, unitPosition - unit.size * 2, unit.size * 4.0)
                    .sumByDouble { it.damage * 1.0 }

    score +=
            explosionsIntersectingWithUnitAtPosition(unitPosition)
                    .sumByDouble { it.explosionParams.damage * 2.0 }

    score +=
            explosionsIntersectingWithUnitAtPosition(unitPosition - unit.size, unit.size * 2.0)
                    .sumByDouble { it.explosionParams.damage * 1.5 }

    score +=
            explosionsIntersectingWithUnitAtPosition(unitPosition - unit.size * 2, unit.size * 4.0)
                    .sumByDouble { it.explosionParams.damage * 1.0 }

    score += mines
            .map { it to unit.distance(it) }
            .sumByDouble { (mine, distance) ->
                when {
                    distance <= properties.mineExplosionParams.radius -> mine.explosionParams.damage * 2.0
                    distance <= properties.mineExplosionParams.radius * 2.0 -> mine.explosionParams.damage * 1.0
                    distance <= properties.mineExplosionParams.radius * 3.0 -> mine.explosionParams.damage * 0.5
                    else -> 0.0
                }
            }
    return score
}

private fun intersectionWithUnitsScore(unit: Unit, coord: Vec2Double): Double =
        game.units
                .filter { it.id != unit.id }.map { otherUnit ->
                    when {
                        otherUnit.intersects(
                                coord.copy(x = coord.x - properties.unitSize.x * 0.5),
                                properties.unitSize
                        ) -> 10000.0
                        otherUnit.intersects(
                                Vec2Double(coord.x - properties.unitSize.x * 1.5, coord.y - properties.unitSize.y),
                                properties.unitSize * 2
                        ) -> 5000.0
                        otherUnit.intersects(
                                Vec2Double(coord.x - properties.unitSize.x * 2.5, coord.y - properties.unitSize.y * 2),
                                properties.unitSize * 4
                        ) -> 2500.0
                        otherUnit.intersects(
                                Vec2Double(coord.x - properties.unitSize.x * 3.5, coord.y - properties.unitSize.y * 3),
                                properties.unitSize * 6
                        ) -> 1250.0
                        else -> 0.0
                    }
                }.sum()


