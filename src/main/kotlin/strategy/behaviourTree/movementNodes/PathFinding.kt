package strategy.behaviourTree.movementNodes

import debugPackage.debug
import model.*
import model.Unit
import strategy.graph
import strategy.math.manhattanDistance
import strategy.properties
import java.util.*
import kotlin.collections.HashMap

fun findRoute(from: Vec2Double, to: Vec2Double, unit: Unit): List<Vertex> {
    debug.drawPoint(unit.position, 0.3f, ColorFloat(r = 255f, g = 255f))
    debug.drawPoint(to, 0.3f, ColorFloat(r = 255f))

    val toVertex = graph.vertexes
            .minBy { manhattanDistance(to, it.coord) }!!

    val route =
            graph.vertexes
                    .filter { manhattanDistance(it.coord, from) < 1.0 }
                    .map { fVertex ->
                        findRoute(fVertex, toVertex, unit)
                    }
                    .minBy { it.size }!!

//    route.asSequence().windowed(2).forEach { (first, second) ->
//        debug.draw(CustomData.Line(
//                first.coord.toFloat(),
//                second.coord.toFloat(),
//                0.1f,
//                ColorFloat(r = 255f))
//        )
//    }

    return when {
        route.size > 1 -> route.drop(1)
        else -> route
    }
}

fun findRoute(from: Vertex, to: Vertex, unit: Unit): List<Vertex> {
    val node2RouteNode = HashMap<Vertex, RouteNode>()
    val queue = PriorityQueue<RouteNode>()
    val start = RouteNode(from,
            null, from.damageImpactScore, manhattanDistance(from.coord, to.coord))

    node2RouteNode[from] = start
    queue.add(start)

    while (!queue.isEmpty()) {
        val next = queue.poll()

        val currentList = LinkedList<Vertex>()
        var cur: RouteNode? = next
        while (cur != null) {
            currentList.addFirst(cur.current)
            cur = cur.previous
        }

//        debug.draw(CustomData.PlacedText(counter.toString(), next.current.coord.toFloat(), TextAlignment.CENTER, 15.0f, ColorFloat(r = 255f)))
        if (next.routeScore > 10000000.0) return emptyList()
        if (next.current == to) {
            val route = currentList

            // debug start
//            node2RouteNode.forEach { (t, u) ->
//                debug.draw(CustomData.PlacedText(
//                        if (u.estimatedScore > 1000000) "-" else ((u.estimatedScore * 100).toInt() / 100.0).toString(),
//                        t.coord.toFloat(), TextAlignment.CENTER, 12.5f, ColorFloat(g = 255f)))
//            }

//            println("###########################")
//            println("FUCKING INPUT")
//            println("can stop jump " + unit.jumpState.canCancel)
//            println("max time " + unit.jumpState.maxTime.toString())
//            println("###########################")

            // debug end

            return route
        }

        graph.edges[next.current].orEmpty()
                .asSequence()
                // not going for same node twice
                .filter { !currentList.contains(it.key) }
                .forEach { (neighbourVertex, _) ->
                    val nextNode = node2RouteNode.computeIfAbsent(neighbourVertex) { x -> RouteNode(x) }

                    val correct = checkIsReachable(LinkedList(currentList + neighbourVertex), unit.jumpState.maxTime, unit.jumpState.speed)

                    val newScore = if (!correct)
                        1000000.0
                    else
                        next.routeScore +
                                manhattanDistance(next.current.coord, neighbourVertex.coord) +
                                neighbourVertex.otherUnitsCollisionScore +
                                neighbourVertex.damageImpactScore -
                                if (neighbourVertex.tile == Tile.JUMP_PAD) 1.0 else 0.0

                    if (nextNode.routeScore > newScore) {
                        nextNode.previous = next
                        nextNode.routeScore = newScore
                        nextNode.estimatedScore = newScore +
                                manhattanDistance(neighbourVertex.coord, to.coord)

                        queue.add(nextNode)
                    }
                }
    }

    debug.drawPoint(start.current.coord, 0.3f, ColorFloat(r = 255f))
    debug.drawPoint(to.coord, 0.3f, ColorFloat(g = 255f))

    println("No route found from ${start.current} to ${to}")
    return emptyList()
//    throw IllegalStateException("No route found from ${start.current} to ${to}")
}

private fun checkIsReachable(nodes: LinkedList<Vertex>,
                             startMaxTime: Double,
                             startJumpSpeed: Double): Boolean {
    var maxTime = startMaxTime
    var jumpSpeed = startJumpSpeed

    var curVertex = nodes.poll()
    while (nodes.isNotEmpty()) {
        val nextVertex = nodes.poll()

        val nextTileUnder = nextVertex.tileUnder
        val (nextCoord, nextTile) = nextVertex
        //if going down but can't go down, fail check
        if (nextCoord.y < curVertex.coord.y) {
            if (jumpSpeed == properties.jumpPadJumpSpeed && maxTime > 0.3)
                return false
            maxTime = 0.0
        }

        if (nextCoord.y > curVertex.coord.y) {
            //if going up and it is not possible, fail check
            val shiftYTime = (nextCoord.y - curVertex.coord.y) / jumpSpeed

            maxTime -= shiftYTime
            if (maxTime < 0.0) {
                return false
            }
        }

        if (nextTile == Tile.JUMP_PAD || nextVertex.tileUnder == Tile.JUMP_PAD) {
            maxTime = properties.jumpPadJumpTime
            jumpSpeed = properties.jumpPadJumpSpeed
        } else if (nextTile == Tile.LADDER
                || nextTileUnder == Tile.WALL
                || nextTileUnder == Tile.PLATFORM) {
            maxTime = properties.unitJumpTime
            jumpSpeed = properties.unitJumpSpeed
        }

        curVertex = nextVertex
    }

    return true
}

data class RouteNode(var current: Vertex,
                     var previous: RouteNode? = null,
                     var routeScore: Double = Double.POSITIVE_INFINITY,
                     var estimatedScore: Double = Double.POSITIVE_INFINITY) : Comparable<RouteNode> {
    override fun compareTo(other: RouteNode): Int =
            (this.estimatedScore - other.estimatedScore).toInt()
}
