package strategy.behaviourTree.evasion

import model.Unit
import model.UnitAction
import model.Vec2Double
import strategy.behaviourTree.BTreeNode
import strategy.behaviourTree.movementNodes.Move
import strategy.behaviourTree.movementNodes.findRoute
import strategy.extensions.distance
import strategy.graph
import strategy.math.manhattanDistance

class MoveWithEvasion(val destination: (Unit) -> Vec2Double) : BTreeNode() {
    override fun eval(unit: Unit): UnitAction {
        val targetVertex = findRoute(unit.position, destination(unit), unit)
                .mapIndexed { i, vertex -> i to vertex }
                .filter { unit.distance(it.second) < 20.0 }
                .maxBy { it.first }?.second

        val places = graph.vertexes.filter { unit.distance(it) < 20.0 }
                .map { v ->
                    v to v.damageImpactScore +
                            (targetVertex?.let { manhattanDistance(it.coord, v.coord) } ?: 0.0)
                }
//
//        places.forEach { (t, u) ->
//            debug.draw(CustomData.PlacedText(
//                    ((u * 100).toInt() / 100.0).toString(),
//                    t.coord.toFloat(), TextAlignment.CENTER, 12.5f, ColorFloat(g = 255f)))
//        }
//
        val bestScore = places.minBy { it.second }!!.second
        val safestPlace = places
                .filter { it.second == bestScore }
                .minBy { unit.distance(it.first) }!!.first

        return Move { safestPlace.coord }.eval(unit)
    }

}