package strategy.behaviourTree

import model.Unit
import model.UnitAction
import model.Vec2Double

abstract class BTreeNode {
    abstract fun eval(unit: Unit): UnitAction
}

class Selector<T : BTreeNode>(private val chooses: List<Pair<(Unit) -> Boolean, T>>, private val or: T? = null) : BTreeNode() {
    constructor(or: T? = null, vararg parts: Pair<(Unit) -> Boolean, T>) : this(parts.toList(), or)
    constructor(or: T? = null, part: Pair<(Unit) -> Boolean, T>) : this(listOf(part), or)

    override fun eval(unit: Unit): UnitAction {
        val branch = chooses.firstOrNull { (condition, _) -> condition(unit) }?.second ?: or
        ?: throw RuntimeException("None of chooses are applicable and or is null")

        return branch.eval(unit)
    }
}

class Concurrent<T : BTreeNode>(
        private val parts: List<T>,
        private val reduceVelocity: (Double, Double) -> Double = { l, r -> if (l == 0.0) r else l },
        private val reduceJump: (Boolean, Boolean) -> Boolean = { l, r -> l || r },
        private val reduceJumpDown: (Boolean, Boolean) -> Boolean = { l, r -> l || r },
        private val reduceAim: (Vec2Double, Vec2Double) -> Vec2Double = { l, r -> if (l.x == 0.0 && l.y == 0.0) r else l },
        private val reduceShoot: (Boolean, Boolean) -> Boolean = { l, r -> l || r },
        private val reduceSwapWeapon: (Boolean, Boolean) -> Boolean = { l, r -> l || r },
        private val reducePlantMine: (Boolean, Boolean) -> Boolean = { l, r -> l || r },

        val reduce: (UnitAction, UnitAction) -> UnitAction = { l, r ->
            UnitAction(
                    velocity = reduceVelocity(l.velocity, r.velocity),
                    jump = reduceJump(l.jump, r.jump),
                    jumpDown = reduceJumpDown(l.jumpDown, r.jumpDown),
                    aim = reduceAim(l.aim, r.aim),
                    shoot = reduceShoot(l.shoot, r.shoot),
                    swapWeapon = reduceSwapWeapon(l.swapWeapon, r.swapWeapon),
                    plantMine = reducePlantMine(l.plantMine, r.plantMine)
            )
        }
) : BTreeNode() {
    constructor(vararg parts: T) : this(parts.toList())

    override fun eval(unit: Unit): UnitAction = parts
            .map { it.eval(unit) }
            .reduce { acc, t -> reduce(acc, t) }
}


object EmptyNode : BTreeNode() {
    override fun eval(unit: Unit): UnitAction = UnitAction()
}
