package strategy.behaviourTree

import model.Item
import model.Unit
import model.UnitAction
import model.WeaponType
import strategy.behaviourTree.attack.Attack
import strategy.behaviourTree.attack.RocketAttack
import strategy.behaviourTree.evasion.MoveWithEvasion
import strategy.behaviourTree.movementNodes.GoForHealthPack
import strategy.behaviourTree.movementNodes.Hide
import strategy.behaviourTree.movementNodes.MoveTowardsFiringPosition
import strategy.extensions.distance
import strategy.weaponPacks

object SwapWeapon : BTreeNode() {
    override fun eval(unit: Unit): UnitAction = UnitAction(swapWeapon = true)
}


val moveToNearestWeapon = MoveWithEvasion { unit: Unit ->
    weaponPacks
            .filter { (it.item as Item.Weapon).weaponType != WeaponType.ROCKET_LAUNCHER }
            .minBy { unit.distance(it) }!!.position
}

val tree: BTreeNode = Selector(
        or = Concurrent(
                Selector(
                        listOf(
                                { unit: Unit -> unit.weapon == null || unit.weapon?.type == WeaponType.ROCKET_LAUNCHER } to
                                        Concurrent(
                                                moveToNearestWeapon,
                                                SwapWeapon
                                        ),
                                Hide.isApplicable to Hide,
                                GoForHealthPack.isApplicable to GoForHealthPack
                        ),
                        or = Concurrent(
                                MoveTowardsFiringPosition
                        )
                ),
                Selector(
                        Attack,
                        RocketAttack.isApplicable to RocketAttack
                )
        ),
        part = { unit: Unit -> unit.weapon == null } to MoveWithEvasion { unit: Unit ->
            weaponPacks
                    .minBy { unit.distance(it) }!!.position
        }


)


