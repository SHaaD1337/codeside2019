package strategy.extensions

import model.*
import model.Unit
import strategy.behaviourTree.movementNodes.Vertex
import strategy.bullets.ExplosionPoint
import strategy.math.distanceSqr

fun Unit.distance(pos: Vec2Double): Double {
    return distanceSqr(this.position, pos)
}

fun Unit.distance(box: LootBox): Double {
    return distance(box.position)
}

fun Unit.distance(unit: Unit): Double {
    return distance(unit.position)
}

fun Unit.distance(vertex: Vertex): Double {
    return distance(vertex.coord)
}

fun Unit.distance(ep: ExplosionPoint): Double {
    return distance(ep.position)
}

fun Unit.distance(mine: Mine): Double {
    return distance(mine.position)
}

fun Unit.distance(bullet: Bullet): Double {
    return distance(bullet.position)
}
