package strategy.extensions

import model.Vec2Double
import model.Vec2Float
import strategy.properties

fun Vec2Float.perTick() = Vec2Float(
        this.x / properties.ticksPerSecond.toFloat(), this.y / properties.ticksPerSecond.toFloat())

fun Double.perTick() =
        this / properties.ticksPerSecond

fun Vec2Double.perTick() = Vec2Double(
        this.x / properties.ticksPerSecond, this.y / properties.ticksPerSecond)