package strategy.math

import model.Tile
import model.Vec2Double
import model.plus
import strategy.level
import strategy.properties
import kotlin.math.abs
import kotlin.system.measureNanoTime

fun distanceSqr(a: Vec2Double, b: Vec2Double): Double {
    return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)
}

fun manhattanDistance(a: Vec2Double, b: Vec2Double) =
        abs(a.x - b.x) + abs(a.y - b.y)

fun intersects(l1: Vec2Double, size1: Vec2Double, l2: Vec2Double, size2: Vec2Double): Boolean {
    // If one rectangle is on left side of other
    if (l1.x > l2.x + size2.x || l2.x > l1.x + size1.x) {
        return false
    }

    return !(l1.y > l2.y + size2.y || l2.y > l1.y + size1.y)
}

fun contains(l: Vec2Double, size: Vec2Double, point: Vec2Double): Boolean =
        l.x <= point.x && point.x <= l.x + size.x
                && l.y <= point.y && point.y <= l.y + size.y


fun noObstaclesToEnemy(unitPosition: Vec2Double, target: Vec2Double): Boolean {
    val bulletStartingPoint = unitPosition.copy(y = unitPosition.y + properties.unitSize.y / 2)

    val targetMiddle = target.copy(y = target.y + properties.unitSize.y / 2.0)

    return !hasObstaclesBetween(bulletStartingPoint, targetMiddle)
}

fun hasObstaclesBetween(a: Vec2Double, b: Vec2Double): Boolean {
    val steps = 100

    val (x, y) = a

    val dx = (b.x - x) / steps
    val dy = (b.y - y) / steps

    for (i in 1 until steps) {
        val tile = level.tiles[(x + dx * i).toInt()][(y + dy * i).toInt()]
        if (tile == Tile.WALL) return true
    }

    return false
}

fun intersectsLineRect(l1: Vec2Double, l2: Vec2Double, r1: Vec2Double, rSize: Vec2Double): Boolean {
    val (x1, y1) = l1
    val (x2, y2) = l2
    val (minX, minY) = r1
    val maxX = minX + rSize.x
    val maxY = minY + rSize.y

    // Completely outside.
    if (x1 <= minX && x2 <= minX || y1 <= minY && y2 <= minY || x1 >= maxX && x2 >= maxX || y1 >= maxY && y2 >= maxY)
        return false

    val m = (y2 - y1) / (x2 - x1)

    var y = m * (minX - x1) + y1
    if (y > minY && y < maxY) return true

    y = m * (maxX - x1) + y1
    if (y > minY && y < maxY) return true

    var x = (minY - y1) / m + x1
    if (x > minX && x < maxX) return true

    x = (maxY - y1) / m + x1
    return x > minX && x < maxX
}

fun rotatePoint(center: Vec2Double, angle: Double, distance: Double): Vec2Double {
    return center + Vec2Double(Math.cos(angle) * distance, Math.sin(angle) * distance)
}


fun intersectsPointTriangle(pt: Vec2Double, v1: Vec2Double, v2: Vec2Double, v3: Vec2Double): Boolean {
    fun sign(p1: Vec2Double, p2: Vec2Double, p3: Vec2Double) =
            (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y)

    val d1 = sign(pt, v1, v2)
    val d2 = sign(pt, v2, v3)
    val d3 = sign(pt, v3, v1)
    val hasNeg = d1 < 0 || d2 < 0 || d3 < 0
    val hasPos = d1 > 0 || d2 > 0 || d3 > 0

    return !(hasNeg && hasPos)
}

// -------------------- Unrelated to game mechanic
fun <T> measureAndPrintTime(actionName: String = "", action: () -> T): T {
    var res: T? = null
    val time = measureNanoTime { res = action() }
    println("$actionName $time")
    return res!!
}