package strategy.bullets

import debugPackage.debug
import model.*
import model.Unit
import strategy.currentTick
import strategy.enemyUnits
import strategy.extensions.perTick
import strategy.level
import strategy.math.intersects
import strategy.math.rotatePoint
import strategy.properties

const val bulletPredictionNextTicks = 30

private lateinit var tick2BulletState: Map<Int, BulletState>

private lateinit var tick2ExplosionState: Map<Int, MutableList<ExplosionPoint>>

data class BulletState(val bullets: List<Bullet>)

data class ExplosionPoint(val position: Vec2Double, val explosionParams: ExplosionParams)

lateinit var bulletsForNextTicks: List<Bullet>
lateinit var explosionsForNextTicks: List<ExplosionPoint>

fun updateBulletPrediction() {
    val currentTick2BulletState = mutableMapOf(currentTick to BulletState(strategy.bullets))
    val currentTick2ExplosionState = hashMapOf<Int, MutableList<ExplosionPoint>>()

    val enemyUnitsLocal = enemyUnits.filter { it.weapon != null }.map {
        Unit().apply {
            id = it.id
            playerId = it.playerId
            weapon = it.weapon
            position = it.position
            size = it.size
        }
    }

    (currentTick + 1..currentTick + bulletPredictionNextTicks).forEach { tick ->
        val (bullets) = currentTick2BulletState[tick - 1]!!

        val newBullets2IsCollapsed = bullets.map { bullet ->
            val velocityPerTick = bullet.velocity.perTick()

            val newPosition = bullet.position.copy(
                    x = bullet.position.x + velocityPerTick.x,
                    y = bullet.position.y + velocityPerTick.y
            )

            bullet.copy(position = newPosition)
        }.map { bullet ->
            bullet to bulletCollidesWithTiles(bullet)
        }

        val predictedRockets = enemyUnitsLocal
                .filter { it.weapon?.type == WeaponType.ROCKET_LAUNCHER }
                .mapNotNull { enemy ->
                    if (enemy.weapon!!.fireTimer == 0.0 || enemy.weapon!!.fireTimer == null) {
                        enemy.weapon!!.fireTimer = enemy.weapon!!.params.fireRate
                        return@mapNotNull Bullet(enemy.weapon!!.type, enemy.id, enemy.playerId, enemy.center,
                                getBulletVelocity(enemy, enemy.weapon!!.lastAngle
                                        ?: 0.0, enemy.weapon!!.params.bullet.speed),
                                enemy.weapon!!.params.bullet.damage,
                                enemy.weapon!!.params.bullet.size,
                                enemy.weapon!!.params.explosion
                        )
                    } else {
                        enemy.weapon!!.fireTimer = enemy.weapon!!.fireTimer!! - 1.0
                    }
                    null
                }

        currentTick2BulletState[tick] = BulletState(
                newBullets2IsCollapsed.filter { !it.second }.map { it.first } + predictedRockets
        )

        newBullets2IsCollapsed
                .asSequence()
                .filter { (_, collapsed) -> collapsed }
                .filter { it.first.explosionParams != null }
                .forEach { (bullet, _) ->
                    currentTick2ExplosionState.computeIfAbsent(tick) { ArrayList() }
                            .add(ExplosionPoint(bullet.position.roundTo(5), bullet.explosionParams!!))
                }
    }

    tick2BulletState = currentTick2BulletState
    tick2ExplosionState = currentTick2ExplosionState

    // debug
//    tick2BulletState.forEach { (_, state) ->
//        state.bullets.forEach { b ->
//            debug.draw(
//                    CustomData.Rect(
//                            (b.position - b.size / 2.0).toFloat(),
//                            Vec2Double(b.size, b.size).toFloat(),
//                            ColorFloat(r = 255f, g = 255f)
//                    )
//            )
//        }
//    }
    // end debug

    tick2ExplosionState.values.flatten().distinctBy { it.position }.forEach { (coord, params) ->
        debug.draw(
                CustomData.Rect(
                        Vec2Double(coord.x - params.radius, coord.y - params.radius).toFloat(),
                        Vec2Double(params.radius * 2, params.radius * 2).toFloat(),
                        ColorFloat(r = 200f, a = 0.5f)
                )
        )
    }

    val (bullets, explosions) = (0..bulletPredictionNextTicks).map { currentTick + it }
            .map { getBulletStateForTick(it) to getExplosionsForTick(it) }
            .fold(emptyList<Bullet>() to emptyList<ExplosionPoint>()) { l, r ->
                (l.first + r.first.bullets) to (l.second + r.second)
            }

    bulletsForNextTicks = bullets
    explosionsForNextTicks = explosions
}

fun getBulletVelocity(unit: Unit, angle: Double, speed: Double) = rotatePoint(unit.center, angle, speed) - unit.center

fun bulletCollidesWithTiles(bullet: Bullet): Boolean {
    val actualPosition = Vec2Double(
            bullet.position.x - bullet.size / 2.0,
            bullet.position.y - bullet.size / 2.0
    )
    val (x, y) = actualPosition.x.toInt() to actualPosition.y.toInt()

    val tilesAround = listOf(
            x - 1 to y + 1, x to y + 1, x + 1 to y + 1,
            x - 1 to y, x to y, x + 1 to y,
            x - 1 to y - 1, x to y - 1, x + 1 to y - 1
    )
            .filter { (x, y) ->
                x >= 0 && x < level.tiles.size &&
                        y >= 0 && y < level.tiles[x].size
            }

    return tilesAround.any { (x, y) ->
        level.tiles[x][y] == Tile.WALL && intersects(actualPosition, bullet.vecSize,
                Vec2Double(x, y), Tile.WALL.size)
    }
}

fun getBulletStateForTick(tick: Int) =
        tick2BulletState[tick] ?: tick2BulletState.maxBy { it.key }!!.value

fun getExplosionsForTick(tick: Int) =
        tick2ExplosionState[tick] ?: tick2ExplosionState.maxBy { it.key }?.value.orEmpty()

fun bulletsIntersectingWithUnitAtPosition(unit: model.Unit, pos: Vec2Double, unitSize: Vec2Double = properties.unitSize) = bulletsForNextTicks
        .filter { it.unitId != unit.id }
        .filter { b ->
            intersects(
                    Vec2Double(b.position.x - b.size / 2.0, b.position.y - b.size / 2.0),
                    Vec2Double(b.size, b.size),
                    pos,
                    unitSize
            )
        }.distinctBy { it.id }

fun explosionsIntersectingWithUnitAtPosition(pos: Vec2Double, unitSize: Vec2Double = properties.unitSize) = explosionsForNextTicks.filter { (expPos, b) ->
    intersects(
            Vec2Double(expPos.x - b.radius, expPos.y - b.radius),
            Vec2Double(b.radius * 2.0, b.radius * 2.0),
            pos,
            unitSize
    )
}.distinct()