package strategy

import model.*
import strategy.behaviourTree.movementNodes.MovementGraph

lateinit var properties: Properties

lateinit var game: Game

var myPlayerId: Int = 0
var currentTick: Int = 0

lateinit var units: List<model.Unit>
lateinit var enemyUnits: List<model.Unit>
lateinit var myUnits: List<model.Unit>

lateinit var bullets: List<Bullet>
lateinit var mines: List<Mine>

lateinit var healthPacks: List<LootBox>
lateinit var weaponPacks: List<LootBox>
lateinit var minePacks: List<LootBox>

lateinit var level: Level

lateinit var graph: MovementGraph

fun initState(playerView: PlayerView) {
    properties = playerView.game.properties
    game = playerView.game
    myPlayerId = playerView.myId

    units = playerView.game.units.toList()
    enemyUnits = playerView.game.units.filter { it.playerId != myPlayerId }
    myUnits = playerView.game.units.filter { it.playerId == myPlayerId }

    bullets = playerView.game.bullets.toList()

    mines = playerView.game.mines.toList()

    currentTick = playerView.game.currentTick

    level = playerView.game.level

    healthPacks = playerView.game.lootBoxes.filter { it.item is Item.HealthPack }
    weaponPacks = playerView.game.lootBoxes.filter { it.item is Item.Weapon }
    minePacks = playerView.game.lootBoxes.filter { it.item is Item.Mine }

    graph = MovementGraph(level)
}

fun ally(unit: model.Unit): model.Unit? = myUnits.firstOrNull { it.id != unit.id }