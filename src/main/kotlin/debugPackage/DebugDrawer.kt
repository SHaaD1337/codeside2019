package debugPackage

import model.*
import model.Unit
import java.io.OutputStream

class Debug(private val stream: OutputStream) {

    fun draw(customData: model.CustomData) {
        model.PlayerMessageGame.CustomDataMessage(customData).writeTo(stream)
        stream.flush()
    }

    fun drawPoint(p: Vec2Double, size: Float = 0.5f, color: ColorFloat = ColorFloat(g = 255f)) {
        draw(CustomData.Rect(p.toFloat(), Vec2Float(size, size), color))
    }

    fun drawLog(text: String) {
        draw(CustomData.Log(text))
    }

    fun drawLine(p1: Vec2Double, p2: Vec2Double, width: Float = 0.5f, color: ColorFloat = ColorFloat(g = 255f)) {
        draw(CustomData.Line(p1.toFloat(), p2.toFloat(), width, color))
    }
}

lateinit var debug: Debug

fun drawDebug(unit: Unit) {
    //distance
//    debug.draw(CustomData.Log("Distance to enemy " + unit.distance(enemyUnits.first())))

    //spread
//    unit.weapon?.let {
//        debug.draw(CustomData.PlacedText(((unit.weapon!!.spread / unit.weapon!!.params.maxSpread * 100).toInt()).toString(), (unit.center + 2.0).toFloat(), TextAlignment.CENTER, 30f, ColorFloat(g = 255f, r = 255f)))
//    }

    // vertexes
//    graph.vertexes.forEach {
//        debug.drawPoint(it.coord, 0.1f)
//    }
//
    // edges
//    graph.edges.forEach { (v, map) ->
//        map.keys.forEach { v2 ->
//            debug.draw(CustomData.Line(v.coord.toFloat(), v2.coord.toFloat(), 0.1f, ColorFloat(r = 255f)))
//        }
//    }
}
