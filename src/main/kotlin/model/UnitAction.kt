package model

import util.StreamUtil

data class UnitAction(var velocity: Double = 0.0,
                      var jump: Boolean = false,
                      var jumpDown: Boolean = false,
                      var aim: model.Vec2Double = Vec2Double(),
                      var shoot: Boolean = false,
                      var reload:Boolean = false,
                      var swapWeapon: Boolean = false,
                      var plantMine: Boolean = false) {
    companion object {
        fun readFrom(stream: java.io.InputStream): UnitAction {
            return UnitAction(
                    velocity = StreamUtil.readDouble(stream),
                    jump = StreamUtil.readBoolean(stream),
                    jumpDown = StreamUtil.readBoolean(stream),
                    aim = model.Vec2Double.readFrom(stream),
                    shoot = StreamUtil.readBoolean(stream),
                    reload = StreamUtil.readBoolean(stream),
                    swapWeapon = StreamUtil.readBoolean(stream),
                    plantMine = StreamUtil.readBoolean(stream)
            )
        }
    }

    fun writeTo(stream: java.io.OutputStream) {
        StreamUtil.writeDouble(stream, velocity)
        StreamUtil.writeBoolean(stream, jump)
        StreamUtil.writeBoolean(stream, jumpDown)
        aim.writeTo(stream)
        StreamUtil.writeBoolean(stream, shoot)
        StreamUtil.writeBoolean(stream, reload)
        StreamUtil.writeBoolean(stream, swapWeapon)
        StreamUtil.writeBoolean(stream, plantMine)
    }
}
