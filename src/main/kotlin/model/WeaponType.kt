package model

enum class WeaponType(var discriminant: Int) {
    PISTOL(0),
    ASSAULT_RIFLE(1),
    ROCKET_LAUNCHER(2)
}
