package model

import util.StreamUtil
import java.lang.Math.pow

fun Vec2Double.toFloat() = Vec2Float(this.x.toFloat(), this.y.toFloat())
fun Vec2Double.roundTo(sign: Int) = Vec2Double((this.x * pow(10.0, sign.toDouble())).toInt() * 1.0 / pow(10.0, sign.toDouble()),
        (this.y * pow(10.0, sign.toDouble())).toInt() * 1.0 / pow(10.0, sign.toDouble()))

operator fun Vec2Double.minus(z: Double) = Vec2Double(this.x - z, this.y - z)
operator fun Vec2Double.minus(z: Vec2Double) = Vec2Double(this.x - z.x, this.y - z.y)
operator fun Vec2Double.plus(z: Vec2Double) = Vec2Double(this.x + z.x, this.y + z.y)
operator fun Vec2Double.plus(z: Double) = Vec2Double(this.x + z, this.y + z)
operator fun Vec2Double.times(z: Double) = Vec2Double(this.x * z, this.y * z)
operator fun Vec2Double.times(z: Int) = Vec2Double(this.x * z, this.y * z)

fun Vec2Double.pY(y: Double) = Vec2Double(this.x, this.y + y)
fun Vec2Double.pX(x: Double) = Vec2Double(this.x + x, this.y)
fun Vec2Double.mY(y: Double) = Vec2Double(this.x, this.y - y)
fun Vec2Double.mX(x: Double) = Vec2Double(this.x - x, this.y)

data class Vec2Double(var x: Double = 0.0,
                      var y: Double = x) : Comparable<Vec2Double> {
    override fun compareTo(other: Vec2Double): Int {
        val difX = (this.x - other.x).toInt()
        if (difX != 0) return difX
        return (this.y - other.y).toInt()
    }

    constructor(x: Int, y: Int) : this(x.toDouble(), y.toDouble())
    constructor(x: Double, y: Int) : this(x, y.toDouble())
    constructor(x: Int, y: Double) : this(x.toDouble(), y)

    companion object {
        @Throws(java.io.IOException::class)
        fun readFrom(stream: java.io.InputStream): Vec2Double {
            val result = Vec2Double()
            result.x = StreamUtil.readDouble(stream)
            result.y = StreamUtil.readDouble(stream)
            return result
        }
    }

    @Throws(java.io.IOException::class)
    fun writeTo(stream: java.io.OutputStream) {
        StreamUtil.writeDouble(stream, x)
        StreamUtil.writeDouble(stream, y)
    }
}
