package model

import util.StreamUtil
import java.io.IOException
import java.util.concurrent.atomic.AtomicLong

private val bulletCounter = AtomicLong(0)

data class Bullet(
        var weaponType: model.WeaponType = WeaponType.ROCKET_LAUNCHER,
        var unitId: Int = 0,
        var playerId: Int = 0,
        var position: model.Vec2Double = Vec2Double(),
        var velocity: model.Vec2Double = Vec2Double(),
        var damage: Int = 0,
        var size: Double = 0.0,
        var explosionParams: model.ExplosionParams? = null,
        val id: Long = bulletCounter.incrementAndGet()) {

    val bottomLeft by lazy { position - size / 2.0 }
    val vecSize by lazy { Vec2Double(size, size) }

    companion object {
        @Throws(java.io.IOException::class)
        fun readFrom(stream: java.io.InputStream): Bullet = Bullet(
                weaponType = when (StreamUtil.readInt(stream)) {
                    0 -> WeaponType.PISTOL
                    1 -> WeaponType.ASSAULT_RIFLE
                    2 -> WeaponType.ROCKET_LAUNCHER
                    else -> throw IOException("Unexpected discriminant value")
                },
                unitId = StreamUtil.readInt(stream),
                playerId = StreamUtil.readInt(stream),
                position = Vec2Double.readFrom(stream),
                velocity = Vec2Double.readFrom(stream),
                damage = StreamUtil.readInt(stream),
                size = StreamUtil.readDouble(stream),
                explosionParams = if (StreamUtil.readBoolean(stream)) ExplosionParams.readFrom(stream) else null)
    }

    @Throws(java.io.IOException::class)
    fun writeTo(stream: java.io.OutputStream) {
        StreamUtil.writeInt(stream, weaponType.discriminant)
        StreamUtil.writeInt(stream, unitId)
        StreamUtil.writeInt(stream, playerId)
        position.writeTo(stream)
        velocity.writeTo(stream)
        StreamUtil.writeInt(stream, damage)
        StreamUtil.writeDouble(stream, size)
        val explosionParams = explosionParams;
        if (explosionParams == null) {
            StreamUtil.writeBoolean(stream, false)
        } else {
            StreamUtil.writeBoolean(stream, true)
            explosionParams.writeTo(stream)
        }
    }
}
